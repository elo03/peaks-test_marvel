<?php

namespace App\Controller;

use App\Entity\Characters;
use App\Form\CharacterType;
use App\Repository\CharactersRepository;
use App\Services\LoadCharacters;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CharactersController extends AbstractController
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $characters = $this->em->getRepository(Characters::class)->findAll();
        return $this->render('characters/index.html.twig', [
            'controller_name' => 'CharactersController',
            'characters' => $characters
        ]);
    }

    /**
     * @Route("/reload", name="load_characters")
     * @param LoadCharacters $loadCharacters
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loadCharacters(LoadCharacters $loadCharacters)
    {
        $this->em->getRepository(Characters::class)->deleteAllCharacters();
        $loadCharacters->loadCharacters();
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("edit/{id}", name="edit_character")
     * @param Characters $characters
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Characters $characters, Request $request)
    {
        $form = $this->createForm(CharacterType::class, $characters);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($form->getData());
            $this->em->flush();
            $this->addFlash('success', 'Le personnage a bien été modifié ;)');
            return $this->redirectToRoute('index');
        }
        return $this->render('characters/edit.html.twig', [
            'controller_name' => 'CharactersController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_character")
     * @param Characters $characters
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function delete(Characters $characters)
    {
        $this->em->remove($characters);
        $this->em->flush();
        return $this->redirectToRoute('index');
    }
}
