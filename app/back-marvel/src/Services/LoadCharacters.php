<?php

namespace App\Services;

use App\Entity\Characters;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LoadCharacters {

    protected $params;
    protected $client;
    protected $em;
    protected $serializer;

    public function __construct(ParameterBagInterface $params, HttpClientInterface $client, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->params = $params;
        $this->client = $client;
        $this->em = $em;
        $this->serializer = $serializer;
    }

    public function loadCharacters(){
        $response = $this->client->request('GET', $this->params->get('baseUrl'), [
            'query' => [ 'ts' => time(), 'apikey' => $this->params->get('publicKey'), 'hash' => $this->getHash(), 'offset' => 100, ],
        ]);
        $this->saveCharacters($response->toArray()['data']['results']);
    }

    public function getHash(){
        return md5(time().''.$this->params->get('privateKey').''.$this->params->get('publicKey'));
    }

    public function saveCharacters($characters){
        foreach ($characters as $character){
            $person = $this->serializer->deserialize(json_encode($character), Characters::class, 'json');
            $this->em->persist($person);
        }
        $this->em->flush();
    }
}