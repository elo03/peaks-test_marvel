<?php

namespace App\DataFixtures;

use App\Entity\Characters;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CharactersFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $character = new Characters();
        $character->setName("Peter Parker");
        $character->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec purus tortor, tempor non mauris eu, scelerisque faucibus diam.");
        $character->setThumbnail("test");
        $character->setComics("test");
        $character->setIsFavorite(false);
        $manager->persist($character);
        $manager->flush();
    }
}
