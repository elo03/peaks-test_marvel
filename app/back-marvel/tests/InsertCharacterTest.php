<?php

namespace App\Tests;

use App\DataFixtures\CharactersFixtures;
use App\Repository\CharactersRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

class InsertCharacterTest extends KernelTestCase
{
    use FixturesTrait;

    public function testInsertCharacter()
    {
        self::bootKernel();
        $this->loadFixtures([CharactersFixtures::class]);
        $character = self::$container->get(CharactersRepository::class)->count([]);
        $this->assertEquals(1, $character);
    }
}
