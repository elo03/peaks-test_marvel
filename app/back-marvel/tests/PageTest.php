<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PageTest extends WebTestCase
{
    /**
     * @dataProvider urls
     *
     * @param $url
     */
    public function testAccessToindex($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function urls()
    {
        return [
            ['/'],
            ['/reload'],
        ];
    }


}
