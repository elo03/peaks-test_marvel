import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) {

  }

  getCharacters() {
    return this.http.get(`${environment.apiBaseUrl}/characters`).pipe(map(data => data))
  }
  getCharacter(id) {
    return this.http.get(`${environment.apiBaseUrl}/characters/${id}`).pipe(map(data => data))
  }
  editFavorite(id, body) {
    return this.http.put(`${environment.apiBaseUrl}/characters/${id}`, body).pipe(map(data => data))
  }
}
