import { Component, Input, OnInit } from '@angular/core';
import { faEye, faHeart, faHeartBroken } from '@fortawesome/free-solid-svg-icons';
import { CharacterService } from 'src/app/service/character.service';
@Component({
  selector: 'app-character-item',
  templateUrl: './character-item.component.html',
  styleUrls: ['./character-item.component.css']
})
export class CharacterItemComponent implements OnInit {
  @Input() character;
  faHeart = faHeart;
  faEye = faEye;
  faHeartBroken = faHeartBroken
  constructor(public characterService: CharacterService) { }

  ngOnInit(): void {
  }

  editFavorite(favorite, id){
    this.characterService.editFavorite(id, {isFavorite: favorite}).subscribe((data)=>{
      this.character.isFavorite = favorite
    })
  }

}
