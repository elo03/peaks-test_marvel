import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { CharacterService } from 'src/app/service/character.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css']
})
export class CharacterDetailComponent implements OnInit {
  id: string;
  character: {
    id: any;
    name: any;
    description: any;
    isFavorite: any;
    thumbnail?: string;
    parution: any;
    threeLastComics: any
  };

  constructor(private activateRoute: ActivatedRoute, public characterService: CharacterService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
      this.spinner.show();
      this.activateRoute.paramMap.subscribe(paramMap => {
      this.id = paramMap.get('id');
      this.characterService.getCharacter(this.id).subscribe((character: any) => {
          this.character = {
              thumbnail: character.thumbnail.path+'.'+character.thumbnail.extension,
              id: character.id,
              name: character.name,
              description: character.description,
              isFavorite: character.isFavorite,
              parution: character.comics.returned,
              threeLastComics: character.comics.items.slice(0, 3)
          };
      });
      setTimeout(()=>{
          this.spinner.hide()
        }, 3000)
      });
  }

}
