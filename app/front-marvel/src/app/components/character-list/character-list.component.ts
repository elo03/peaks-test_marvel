import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { CharacterService } from 'src/app/service/character.service';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit {
  characters: any = [];
  charactersList = []
  p: number = 1;
  constructor(public characterService: CharacterService, public spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinner.show();
    this.characterService.getCharacters().subscribe(data=>{
      this.characters = data['hydra:member'].map(character => {
        return {
          id: character.id,
          name: character.name,
          description: character.description,
          isFavorite: character.isFavorite,
          thumbnail: character.thumbnail.path+'.'+character.thumbnail.extension,
          parution: character.comics.returned
        }
      });
      this.charactersList = this.characters;
      this.spinner.hide();
    })
  }

  showFavoris(state){
    state ? this.charactersList = this.characters.filter((character) => {return character.isFavorite == state;}) : this.charactersList = this.characters;;
  }
}
