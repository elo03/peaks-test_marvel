# TEST MARVEL

Cette application permet d'afficher les 20 premiers personnages de Marvel à partir du 100ème de la liste fourni par l'API de marvel (https://developer.marvel.com/)

## Fonctionnalités
 
    - Liste des 20 personnages avec leur nom, image, description et nombre d'apparition.
    - Accéder au détail du personnage
    - Marquer un personnage comme favoris
    - Loader
    - Pagination pour afficher 10 personnages par page
    - 3 premiers films dans lequel le personnage apparait

   Le back en Symfony 4 permet de stocker les personnages recupérer depuis l'API.
   
   Le front en Angular 10 permet à l'utilisateur d'intéaragir avec les personnages récupérés depuis l'API (lire les informations, marquer comme favoris etc).
  
   J'ai choisi d'appréhender la problématique en me focalisant sur le fonctionnel. Ce dernier est priorisé au détriment du design qui pour le coup est un peu minimaliste. 

## Prérequis

- Node v12.19.0 ou +
- npm 6.14.8 ou +
   - Mac: `brew install node`
   - Windows `https://nodejs.org/en/download/`
- Docker desktop: https://docs.docker.com/desktop/ 

- S'assurer que les ports 80, 8080 et 8081 sont disponibles.
## Mis en route du projet - Build
1. Cloner le projet: `git clone git@gitlab.com:elo03/peaks-test_marvel.git`
2. `cd peaks-test_marvel/docker-template`
3. `make build-app`

##### Back - lancement
1. `cd peaks-test_marvel/docker-template`
2. `make run-api-serve` pour lancer le serveur Symfony 4.
3. Accéder à `http://localhost:8000/` et charger la liste des 20 personnages

##### Front
1. `cd peaks-test_marvel/docker-template`
2. `run-front-serve` pour lancer le serveur Symfony 4.
3. `http://localhost:4200/` pour visualiser l'application Angular
    
##### Phpmyadmin
1. `http://localhost:8081/sql.php?db=marvel&table=characters&pos=0`
1. user: `root`
3. password: `root`
## Tests unitaires

##### Back
1. Se rendre dans le dossier du back `cd peaks-test_marvel/app/back-marvel`
2. Lancer la commande `php bin/phpunit`

## Améliorations possibles:

1. Ajouter des tests sur le front
2. Améliorer le design du front
3. Mettre en place un système d’authentification (back et front) pour sécuriser les appels des apis.
4. Piloter le chargement des personnages côté front.
    - Premier lancement: détecter si des personnages sont disponible en base de données sinon faire appel à  l'API de Marvel
    - Mettre un bouton de refresh de la base de données por réinitialiser la liste des personnages.
5. Dockeriser la partie front